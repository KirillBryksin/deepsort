#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import os
import sys



from timeit import time
import warnings

import PIL
import cv2
import numpy
import numpy as np
from PIL import Image



from yolo import YOLO
import json
from pathlib import Path

from deep_sort import preprocessing
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.detection_yolo import Detection_YOLO
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet
import imutils.video
from videocaptureasync import VideoCaptureAsync

pts = []
boxss = []
w2 = []
h2 = []

warnings.filterwarnings('ignore')

# initialize a list of colors to represent each possible class label
np.random.seed(100)
COLORS = np.random.randint(0, 255, size=(200, 3), dtype="uint8")

def main(yolo):
    # Definition of the parameters
    max_cosine_distance = 0.3
    nn_budget = None
    nms_max_overlap = 1.0

    # Deep SORT
    model_filename = 'model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)

    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
    tracker = Tracker(metric)

    tracking = True
    writeVideo_flag = True
    asyncVideo_flag = False

    file_path = sys.argv[1]
    frame_range = sys.argv[2]

    if not os.path.exists('tracks'):
        os.makedirs('tracks')

        
    if asyncVideo_flag:
        video_capture = VideoCaptureAsync(file_path)
    else:
        video_capture = cv2.VideoCapture(file_path)

    if asyncVideo_flag:
        video_capture.start()

    if writeVideo_flag:
        if asyncVideo_flag:
            w = int(video_capture.cap.get(3))
            h = int(video_capture.cap.get(4))
        else:
            w = int(video_capture.get(3))
            h = int(video_capture.get(4))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(Path(file_path).stem + "_new.avi", fourcc, 30, (w, h))
        frame_index = -1
        new_file_name = './tracks/' + Path(file_path).stem + ".json"
        list_file = open(new_file_name, 'w')


    fps = 0.0
    fps_imutils = imutils.video.FPS().start()



    while True:
        ret, frame = video_capture.read()  # frame shape 640*480*3
        if ret != True:
            break





        t1 = time.time()

        image = PIL.Image.fromarray(frame[..., ::-1])  # bgr to rgb
        boxes, confidence, classes = yolo.detect_image(image)

        if tracking:
            features = encoder(frame, boxes)

            detections = [Detection(bbox, confidence, cls, feature) for bbox, confidence, cls, feature in
                          zip(boxes, confidence, classes, features)]
        else:
            detections = [Detection_YOLO(bbox, confidence, cls) for bbox, confidence, cls in
                          zip(boxes, confidence, classes)]

        # Run non-maxima suppression.
        boxes = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        indices = preprocessing.non_max_suppression(boxes, nms_max_overlap, scores)
        detections = [detections[i] for i in indices]

        i = int(0)
        indexIDs = []
        c = []


        if tracking:
            # Call the tracker
            tracker.predict()
            tracker.update(detections)

            for det in detections:
                bbox = det.to_tlbr()
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 255, 255), 2)

            for track in tracker.tracks:
                if not track.is_confirmed() or track.time_since_update > 1:
                    continue
                indexIDs.append(int(track.track_id))
                color = [int(c) for c in COLORS[indexIDs[i] % len(COLORS)]]
                bbox = track.to_tlbr()
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (color), 3)
                cv2.putText(frame, "ID: " + str(track.track_id), (int(bbox[0]), int(bbox[1])), 0,
                            1e-3 * frame.shape[0], (0, 255, 0), 1)


                # bbox_center_point(x,y)

                center = (int(((bbox[0]) + (bbox[2])) / 2), int(((bbox[1]) + (bbox[3])) / 2))
                pts.append(center)
                thickness = 5
                # center point
                cv2.circle(frame, center, 1, color, thickness)



            for det in detections:
                bbox = det.to_tlbr()
                score = "%.2f" % round(det.confidence * 100, 2) + "%"
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 0, 0), 2)
                cv2.putText(frame, score + '%', (int(bbox[0]), int(bbox[3])), 0, 5e-3 * 130, (0,255,0),2)
                boxx = (int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3]))
                boxss.append(boxx)
                w2.append(int(bbox[2]) - bbox[0])
                h2.append(int(bbox[3]) - bbox[1])





        # cv2.imshow('Deepsort', frame)


        if writeVideo_flag:  # and not asyncVideo_flag:
            # save a frame
            out.write(frame)
            frame_index = frame_index + 1
            if len(boxes) != 0:
                if (frame_index % frame_range == 0):
                    for i in range(0, len(boxss)):
                        if not os.path.exists('data/' + Path(file_path).stem + '/index' + str([i])):
                            os.makedirs('data/' + Path(file_path).stem + '/index' + str([i]))
                        frame_crop = image.crop(boxss[i])
                        name = './data/' + Path(file_path).stem + '/index' + str([i]) + '/frame' + str(
                            frame_index) + 'index' + str([i]) + '.jpg'
                        print('Creating...' + name)
                        if (w2[i] >= 64) and (h2[i] >= 64):
                            frame_crop.save(name)

                # for i in range(0, len(indexIDs)):
                #
                    # data = {
                    #     "frame": frame_index,
                    #     "id": indexIDs[i],
                    #     "coordinates": str(pts[i]),
                    #     "bbox": str(boxss[i]),
                    # }
                    # json.dump(data, list_file, indent=2)







        pts.clear()
        boxss.clear()


        fps_imutils.update()




        if not asyncVideo_flag:
            fps = (fps + (1. / (time.time() - t1))) / 2
            print("FPS = %f" % (fps))
            print(str(len(indexIDs)))

        # Press Q to stop!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    fps_imutils.stop()
    print('imutils FPS: {}'.format(fps_imutils.fps()))

    if asyncVideo_flag:
        video_capture.stop()
    else:
        video_capture.release()

    if writeVideo_flag:
        out.release()

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(YOLO())