Deepsort module for cropping images

Before you run tracker, download weights from
https://drive.google.com/file/d/15P4cYyZ2Sd876HKAEWSmeRdFl_j-0upi/view

Copy this to the root folder, then run

python convert.py yolov4.weights model_data/yolo.h5

Then run the script by:

python demo.py "path_to_the_file" "number of frames between cropping"

After running script, you will have a folder with cropped images of cars